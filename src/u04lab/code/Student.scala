package u04lab.code

import Lists._
import u04lab.code.Lists.List.{Cons, Nil, foldLeft, map} // import custom List type (not the one in Scala stdlib)

trait Student {
  def name: String
  def year: Int
  def enrolling(course: Course*): Unit // the student participates to a Course
  def courses: List[String] // names of course the student participates to
  def hasTeacher(teacher: String): Boolean // is the student participating to a course of this teacher?
}

trait Course {
  def name: String
  def teacher: String
}

object Student {
  def apply(name: String, year: Int = 2017): Student = StudentImpl(name, year)
}

object Course {
  def apply(name: String, teacher: String): Course = CourseImpl(name, teacher)
}


/** Hints:
  * - simply implement Course, e.g. with a case class
  * - implement Student with a StudentImpl keeping a private Set of courses
  * - try to implement in StudentImpl method courses with map
  * - try to implement in StudentImpl method hasTeacher with map and find
  * - check that the two println above work correctly
  * - refactor the code so that method enrolling accepts a variable argument Course*
  */

case class CourseImpl(name:String, teacher:String) extends Course

class StudentImpl(override val name : String, override val year: Int) extends Student {
  private var _courses : List[Course] = List.Nil();

  override def enrolling(courses: Course*): Unit = {
    courses.foreach(x => _courses = List.append(_courses, Cons(x, List.Nil())))
  }

  override def courses: List[String] = List.map(_courses)(_.name)

  override def hasTeacher(teacher: String): Boolean = List.contains(List.map(_courses)(_.teacher), teacher)
}

//add companion object and apply because StudentImpl doesn't have one (since not case class)
object StudentImpl{
  def apply(name: String, year: Int) : StudentImpl = new StudentImpl(name, year)
}

//ex 5.
import u04lab.code.Lists._

//cleaner version
object sameTeacher {
  def unapply(courses: List[Course]) : scala.Option[String] = courses match {
      case Cons(h, t) if t == Nil() | scala.Some(h.teacher) == unapply(t) => scala.Some(h.teacher)
      case _ => scala.None
  }
}


//with custom toScalaOption
import u04lab.code.Optionals.Option
object sameTeacher2 {
  def unapply(courses: List[Course]) : scala.Option[String] = courses match {
    case Cons(h, t) if t == Nil() | Option.Some(h.teacher) == unapply(t) => Option.toScalaOption(Option.Some(h.teacher))
    case _ => Option.toScalaOption(Option.None())
  }
}

//version with foldLeft
object sameTeacher3 {
  def unapply(courses: List[Course]) : scala.Option[String] = Option.toScalaOption(
    courses match {
      case Cons(h,t) => foldLeft(map(courses)(_.teacher))(Option.Some[String](h.teacher).asInstanceOf[Optionals.Option[String]])((acc, elem) => (acc, elem) match {
        case (acc, elem) if (acc == Option.Some(elem)) => acc
        case _ => Option.None[String]()
      })
      case _ => Option.None[String]()
    }
  )
}


object Try extends App {
  val cPPS = Course("PPS","Viroli")
  val cOOP = Course("OOP","Viroli")
  val cPCD = Course("PCD","Ricci")
  val cSDR = Course("SDR","D'Angelo")
  val s1 = Student("mario",2015)
  val s2 = Student("gino",2016)
  val s3 = Student("rino") //defaults to 2017
  s1.enrolling(cPPS)
  s1.enrolling(cPCD)
  s2.enrolling(cPPS)
  s3.enrolling(cPPS)
  s3.enrolling(cPCD)
  s3.enrolling(cSDR)
  /*println(s1.courses, s2.courses, s3.courses) // (Cons(PCD,Cons(PPS,Nil())),Cons(PPS,Nil()),Cons(SDR,Cons(PCD,Cons(PPS,Nil()))))
  println(s1.hasTeacher("Ricci")) // true*/

  //test someTeacher
  List(cPPS, cOOP) match {
    case sameTeacher(t) => println ( s"$cOOP , $cPPS have same teacher $t")
  }
}