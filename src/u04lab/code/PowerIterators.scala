package u04lab.code

import Lists._
import u04lab.code.Streams.Stream
import u04lab.code.Optionals.Option

trait PowerIterator[A] {
  def next(): Option[A]
  def allSoFar(): List[A]
  def reversed(): PowerIterator[A]
}

trait PowerIteratorsFactory {
  def incremental(start: Int, successive: Int => Int): PowerIterator[Int]
  def fromList[A](list: List[A]) : PowerIterator[A]
  def randomBooleans(size: Int): PowerIterator[Boolean]
}



//ex. 2
class PowerIteratorsFactoryImpl extends PowerIteratorsFactory {

  override def randomBooleans(size: Int): PowerIterator[Boolean] = {
    val random = scala.util.Random
    this.fromStream(Stream.take(Stream.generate(random.nextBoolean()))(size))
  }

  //incremental and randomBooleans need def : stream => PowerIterator 'cause their laziness, so stream are needed. I use it for list too to refactor code
  override def incremental(start: Int, successive: Int => Int): PowerIterator[Int] = fromStream(Stream.iterate(start)(successive))

  override def fromList[A](list: List[A]): PowerIterator[A]  = fromStream(Stream.fromList(list))

  private def fromStream[A](stream: Stream[A] ) : PowerIterator[A] = new PowerIterator[A] {
    var reversedList: List[A] = List.nil[A]
    var _stream = stream

    def next(): Option[A] = {
      _stream match {
        case Stream.Empty() => Option.None()
        case Stream.Cons(h, t) => {
          reversedList = List.Cons(h(), reversedList);
          _stream = Stream.drop(_stream,1)
          Option.Some[A](h())
        }
      }
    }

    def allSoFar(): List[A] = List.reverse(this.reversedList) //should be unmodifiable
    def reversed(): PowerIterator[A] = fromStream(Stream.fromList(reversedList))
  }
}
